<?php 
	$hash = [];

	//abre o arquivo
	$file = fopen("./resultado.c", "w");
	
	$infoBruta = file_get_contents("./exemplos/media_aluno.alg");

	//trocando "enters" por ";"
	$infoSemQuebras = preg_replace("/\r\n/", ";\n", $infoBruta);

	//removendo "espaços"
	$infoSemQuebras = preg_replace("/\s+/", " ", $infoSemQuebras);

	//replaces gerais
	$infoSemQuebras = preg_replace('/fimalgoritmo;/i', "", $infoSemQuebras);
	$infoSemQuebras = preg_replace("/escreva\(/i", 'printf("%s",', $infoSemQuebras);
	$infoSemQuebras = preg_replace("/<-/", "=", $infoSemQuebras);
	$infoSemQuebras = preg_replace("/:=/", "=", $infoSemQuebras);
	$infoSemQuebras = preg_replace("/leia\(/i", 'scanf("%f",&', $infoSemQuebras);

	//replace manipulando a ordem dos textos, no caso a variável e seu tipo
	$infoSemQuebras = preg_replace_callback('/^algoritmo.*inicio/i', function($macthes) {
		$str = preg_replace('/algoritmo\s+"(.*?)"/i', '', $macthes[0]);
		$str = preg_replace('/inicio$/', '', $str);
		$text = explode(";", $str);

		
		$arr = array_filter($text, function($parameter) {
			return trim($parameter) != '';
		});

		$arr = array_map(function($str) {
			$map = explode(":", $str);
			switch(trim($map[1])) {
				case 'real':
					$tipo = 'float';
					$hash[trim($map[0])] = '"%f"';
					break;
				case 'caracter':
					$tipo = 'char *';
					$hash[trim($map[0])] = '"%s"';
					break;
				case 'inteiro':
					$tipo = 'integer';
					$hash[trim($map[0])] = '"%i"';
					break;
				default:
					$tipo = 'integer';
					$hash[trim($map[0])] = '"%i"';
			}

			return  $tipo.' '.$map[0].';';
		}, $arr);
		$str = implode($arr, " ");
		
		return $str;

	}, $infoSemQuebras);

	$infoSemQuebras = preg_replace_callback('/^algoritmo.*inicio/', function($macthes) {
		
	}, $infoSemQuebras);

	$infoSemQuebras = preg_replace("/( se )/", " if (", $infoSemQuebras);
	$infoSemQuebras = preg_replace("/entao;/i", ") {", $infoSemQuebras);
	$infoSemQuebras = preg_replace("/senao;/i", "} else {", $infoSemQuebras);
	$infoSemQuebras = preg_replace("/inicio;/i", "{", $infoSemQuebras);
	$infoSemQuebras = preg_replace("/fimse/i", "}", $infoSemQuebras);
	$infoSemQuebras = preg_replace("/fim;/i", "}", $infoSemQuebras);

	$infoSemQuebras = preg_replace("/fimenquanto/i", "}", $infoSemQuebras);
	$infoSemQuebras = preg_replace("/enquanto/i", "while (", $infoSemQuebras);
	$infoSemQuebras = preg_replace("/faca/i", ") {", $infoSemQuebras);

	$infoSemQuebras = preg_replace("/;/", ";\n", $infoSemQuebras);

	$codigoFinal = <<<EOF
	#include <stdio.h>

	int main(char * args) {
		$infoSemQuebras	
		return 0;
	} 
EOF;

	//Escreve no arquivo
	fwrite($file, $codigoFinal);

	//Encerra leitura do arquivo;
	fclose($file);
?>